package ru.astafev.xrm.test1.domain;

import lombok.Data;

//import javax.persistence.Id;
//import javax.persistence.MappedSuperclass;
//
//@MappedSuperclass
@Data
public abstract class AbstractPageInfo {
//    @Id
    private Long id;

    private String url;
}
