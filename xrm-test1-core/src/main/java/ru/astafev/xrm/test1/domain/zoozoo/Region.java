package ru.astafev.xrm.test1.domain.zoozoo;

import lombok.Data;

@Data
public class Region {

    String name;

    String url;
}
