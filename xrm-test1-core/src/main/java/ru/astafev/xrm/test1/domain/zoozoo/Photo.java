package ru.astafev.xrm.test1.domain.zoozoo;

import lombok.Data;

@Data
public class Photo {

    Long id;

    byte[] bytes;

    String url;
}
