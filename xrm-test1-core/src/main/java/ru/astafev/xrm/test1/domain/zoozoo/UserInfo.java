package ru.astafev.xrm.test1.domain.zoozoo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.util.Set;

@Data
@EqualsAndHashCode(exclude = "messages")
public class UserInfo {

    Long id;

    Date registrationDate;

    Set<MessageInfo> messages;
}
