package ru.astafev.xrm.test1.domain.zoozoo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.astafev.xrm.test1.domain.AbstractPageInfo;

import java.util.Set;

@Data
@EqualsAndHashCode(callSuper = true)
public class MessageInfo extends AbstractPageInfo {

    String theme;

    UserInfo author;

    SecondLvlCategory category;

    Region region;

    Set<Tag> tags;

}
