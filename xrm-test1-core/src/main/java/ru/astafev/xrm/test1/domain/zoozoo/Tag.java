package ru.astafev.xrm.test1.domain.zoozoo;


import lombok.Data;

@Data
public class Tag {

    Long id;

    String name;

    String url;
}
